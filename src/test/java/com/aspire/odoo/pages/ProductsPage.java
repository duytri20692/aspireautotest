package com.aspire.odoo.pages;

import com.aspire.odoo.enums.Product;
import com.aspire.odoo.helpers.WebElementHelper;
import com.aspire.odoo.models.InventoryProduct;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductsPage extends WebElementHelper {
	private static final Logger LOGGER = Logger.getLogger(ProductsPage.class.getName());

	@FindBy(xpath = "//div[@class=\"o_cp_top_left\"]//span[text() = \"Products\"]")
	WebElement productPage;
	@FindBy(xpath = "//button[@title=\"Create record\"]")
	WebElement createButton;
	@FindBy(xpath = "//button[@title=\"Save record\"]")
	WebElement saveButton;
	@FindBy(xpath = "//button[@title=\"Edit record\"]")
	WebElement editButton;
	@FindBy(xpath = "//button[@name=\"action_update_quantity_on_hand\"]")
	WebElement updateQuantityButton;
	@FindBy(xpath = "//button[@class=\"btn btn-primary o_list_button_add\"]")
	WebElement createQuantityButton;
	@FindBy(xpath = "//a[@title=\"Home menu\"]")
	WebElement homeMenuButton;

	// New Product
	@FindBy(xpath = "//input[@name=\"name\"]")
	WebElement productNameTextBox;
	@FindBy(xpath = "//div[@name=\"sale_ok\"]")
	WebElement canBeSoldCheckbox;
	@FindBy(xpath = "//div[@name=\"purchase_ok\"]")
	WebElement canBePurchasedCheckbox;
	@FindBy(xpath = "//a[text() = \"General Information\"]")
	WebElement generalInformationTab;
	@FindBy(xpath = "//label[normalize-space()='Product Type']/../following-sibling::td//select")
	WebElement productTypeComboBox;
	@FindBy(xpath = "//label[normalize-space()='Unit of Measure']/../following-sibling::td//input")
	WebElement unitOfMeasureComboBox;
	@FindBy(xpath = "//label[normalize-space()='Purchase UoM']/../following-sibling::td//input")
	WebElement purchaseUoMComboBox;
	@FindBy(xpath = "//label[normalize-space()='Sales Price']/../following-sibling::td//input")
	WebElement salesPriceTextBox;
	@FindBy(xpath = "//label[normalize-space()='Cost']/../following-sibling::td//input")
	WebElement costTextBox;
	@FindBy(xpath = "//label[normalize-space()='Product Category']/../following-sibling::td//input")
	WebElement productCategoryComboBox;
	@FindBy(xpath = "//li[@class='ui-menu-item']/a[normalize-space()='All']")
	WebElement productCategoryOptionAll;
	@FindBy(xpath = "//li[@class='ui-menu-item']/a[normalize-space()='All / Expenses']")
	WebElement productCategoryOptionAllExpenses;
	@FindBy(xpath = "//li[@class='ui-menu-item']/a[normalize-space()='All / Saleable']")
	WebElement productCategoryOptionAllSaleable;
	@FindBy(xpath = "//label[normalize-space()='Internal Reference']/../following-sibling::td//input")
	WebElement internalReferenceBox;
	@FindBy(xpath = "//label[normalize-space()='Barcode']/../following-sibling::td//input")
	WebElement barcodeBox;
	@FindBy(xpath = "//div[normalize-space()='Internal Notes']/../../following-sibling::tr//p")
	WebElement internalNotesTextBox;
	@FindBy(xpath = "//input[@name=\"inventory_quantity\"]")
	WebElement countedQuantityTextBox;
	@FindBy(xpath = "//div[@class='o_Message_prettyBody']/p")
	WebElement createProductSuccessMessage;
	@FindBy(xpath = "//p[@class='o_view_nocontent_smiling_face']")
	WebElement emptyProductMessage;

	public ProductsPage(WebDriver driver) {
		super(driver);
	}

	@Step("Click Create Button")
	public void clickCreateButton() {
		waitElementAndClick(createButton, 10);
	}

	@Step("Create Product General Information")
	public void createProductGeneralInformation(InventoryProduct product){
		//Input Product Name
		waitElementVisibilityAndFill(productNameTextBox, 10, product.getProductName());
		//Check on 'Can Be Sold' checkbox
		if(product.isCheckedCanBeSold()){
			if(!canBeSoldCheckbox.isSelected()){
				LOGGER.log(Level.INFO, "'Can Be Sold' Checkbox already checked!");
//				LOGGER.info("'Can Be Sold' Checkbox already checked!");
			}else {
				LOGGER.info("Checking on 'Can Be Sold' Checkbox");
				canBeSoldCheckbox.click();
			}
		}
		//Check on 'Can Be Purchased' checkbox
		if(product.isCheckedCanBePurchased()){
			if(!canBePurchasedCheckbox.isSelected()){
				LOGGER.info("'Can Be Purchased' Checkbox already checked!");
			}else {
				LOGGER.info("Checking on 'Can Be Purchased' Checkbox");
				canBePurchasedCheckbox.click();
			}
		}
		//Click General Information Tab
		generalInformationTab.click();
		//Select Product Type
		Select productTypeCbx = new Select(productTypeComboBox);
		productTypeCbx.selectByVisibleText(product.getProductType());
		//Select Unit of Measure
		unitOfMeasureComboBox.click();
		unitOfMeasureComboBox.clear();
		unitOfMeasureComboBox.sendKeys(product.getUnitOfMeasure());
		unitOfMeasureComboBox.sendKeys(Keys.ENTER);
		//Select Purchase UoM
		purchaseUoMComboBox.click();
		purchaseUoMComboBox.clear();
		purchaseUoMComboBox.sendKeys(product.getPurchaseUoM());
		purchaseUoMComboBox.sendKeys(Keys.ENTER);
		//Input Sale Price
		salesPriceTextBox.clear();
		salesPriceTextBox.sendKeys(product.getSalesPrice());
		//Input Cost
		costTextBox.click();
		costTextBox.clear();
		costTextBox.sendKeys(product.getCost());
		//Select Product Category
		productCategoryComboBox.click();
		switch (product.getProductCategory().trim().toLowerCase().replaceAll(" ","")) {
			case "all":
				waitElementAndClick(productCategoryOptionAll, 5);
				break;
			case "all/expenses":
				waitElementAndClick(productCategoryOptionAllExpenses, 5);
				break;
			case "all/saleable":
				waitElementAndClick(productCategoryOptionAllSaleable, 5);
				break;
			default:
				System.out.println("Invalid Product Category");
		}
		//Input Internal Reference
		internalReferenceBox.sendKeys(product.getInternalReference());
		//Input Barcode
		barcodeBox.sendKeys(product.getBarcode());
		//Input Internal Notes
		internalNotesTextBox.sendKeys(product.getInternalNotes());
	}

	@Step("Click Save Button")
	public void clickSaveButton() {
		saveButton.click();
	}

	@Step("Click Edit Button")
	public void clickEditButton() {
		waitElementAndClick(editButton, 20);
	}

	@Step("Click Update Quantity Button")
	public void clickUpdateQuantityButton() {
		moveToElementAndClick(updateQuantityButton);
	}

	@Step("Click Create Quantity Button")
	public void clickCreateQuantityButton() {
		waitElementAndClick(createQuantityButton, 20);
	}

	@Step("Input Counter Quantity TextBox")
	public void setCountedQuantityTextBox(String quantity) {
		waitElementVisibilityAndFill(countedQuantityTextBox, 20, quantity);
	}

	@Step("Click Home Menu Button")
	public void clickHomeMenuButton() {
		homeMenuButton.click();
	}

	@Step("Check Create Product Success Message")
	public String getCreateProductSuccessMessage(){
		waitElementVisibility(createProductSuccessMessage, 10);
		return createProductSuccessMessage.getText();
	}

	@Step("Check Empty Product Message")
	public String getEmptyProductMessage(){
		waitElementVisibility(emptyProductMessage, 10);
		return emptyProductMessage.getText().trim();
	}

	@Step("Products Page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(productPage, 10);
		return productPage.isDisplayed();
	}
}
