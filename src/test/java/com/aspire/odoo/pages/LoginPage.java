package com.aspire.odoo.pages;

import com.aspire.odoo.helpers.WebElementHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class LoginPage extends WebElementHelper {
	private static final Logger LOGGER = Logger.getLogger(LoginPage.class.getName());

	WebDriver driver;
	@FindBy(xpath = "//*[@id=\"login\"]")
	WebElement emailTextBox;
	@FindBy(xpath = "//*[@id=\"password\"]")
	WebElement passTextBox;
	@FindBy(xpath = "//*[@class=\"btn btn-primary btn-block\"]")
	WebElement logInButton;

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	@Step("Login to web application")
	public void loginToOdoo(String userName, String password){
		emailTextBox.sendKeys(userName);
		passTextBox.sendKeys(password);
		logInButton.click();
	}

	@Step("Login Page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(logInButton, 10);
		return logInButton.isDisplayed();
	}
}
