package com.aspire.odoo.pages;

import com.aspire.odoo.helpers.WebElementHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class MenuPage extends WebElementHelper {
	private static final Logger LOGGER = Logger.getLogger(MenuPage.class.getName());

	WebDriver driver;
	@FindBy(xpath = "//*[text() = 'Inventory']")
	WebElement inventoryCategory;
	@FindBy(xpath = "//*[contains(@class, 'o_caption') and contains(text(), 'Manufacturing')]")
	WebElement manufacturingCategory;

	public MenuPage(WebDriver driver) {
		super(driver);
	}

	@Step("Navigate to `Inventory` feature")
	public void clickInventoryMenu() {
		waitElementAndClick(inventoryCategory, 10);
	}

	@Step("Click Manufacturing Menu")
	public void clickManufacturingMenu() {
		waitElementAndClick(manufacturingCategory, 20);
	}

	@Step("Menu page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(inventoryCategory, 10);
		return inventoryCategory.isDisplayed();
	}
}
