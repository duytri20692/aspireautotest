package com.aspire.odoo.pages;

import com.aspire.odoo.helpers.WebElementHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class InventoryOverviewPage extends WebElementHelper {
	private static final Logger LOGGER = Logger.getLogger(InventoryOverviewPage.class.getName());

	WebDriver driver;
	@FindBy(xpath = "//a[@class=\"dropdown-item o_menu_brand\"][text() = \"Inventory\"]")
	WebElement inventoryHome;
	@FindBy(xpath = "//span[contains(text(),'Products')]")
	WebElement productsMenu;
	@FindBy(xpath = "//a[contains(text(),'Products')]")
	WebElement productsItem;

	public InventoryOverviewPage(WebDriver driver) {
		super(driver);
	}

	@Step("Click Products Menu")
	public void clickProductsMenu() {
		waitElementAndClick(productsMenu, 10);
	}

	@Step("Click Products Item")
	public void clickProductsItem() {
		waitElementAndClick(productsItem, 10);
	}

	@Step("Inventory Overview Page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(inventoryHome, 20);
		return inventoryHome.isDisplayed();
	}
}
