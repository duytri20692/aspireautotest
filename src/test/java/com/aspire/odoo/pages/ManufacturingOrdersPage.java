package com.aspire.odoo.pages;

import com.aspire.odoo.helpers.WebElementHelper;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class ManufacturingOrdersPage extends WebElementHelper {
	private static final Logger LOGGER = Logger.getLogger(ManufacturingOrdersPage.class.getName());

	WebDriver driver;
	@FindBy(xpath = "//a[@class=\"dropdown-item o_menu_brand\"][text() = \"Manufacturing\"]")
	WebElement manufacturingHome;
	@FindBy(xpath = "//button[@data-original-title=\"Create record\"]")
	WebElement createButton;
	@FindBy(xpath = "//label[text() = \"Product\"]/../following-sibling::td//input")
	WebElement productTextBox;
	@FindBy(xpath = "//button[@title=\"Save record\"]")
	WebElement saveButton;
	@FindBy(xpath = "//button[@name=\"action_confirm\"]")
	WebElement confirmButton;
	@FindBy(xpath = "//*[contains(text(),'Mark as Done')]")
	WebElement maskAsDoneButton;
	@FindBy(xpath = "//span[text() = 'Create']")
	WebElement newProductCreateButton;
	@FindBy(xpath = "//span[text() = 'Ok']")
	WebElement OKButton;
	@FindBy(xpath = "//span[text() = 'Apply']")
	WebElement applyButton;
	@FindBy(xpath = "//a[@name=\"product_id\"]")
	WebElement productNameLabel;
	@FindBy(xpath = "//span[@name=\"product_uom_id\"]/span")
	WebElement productQuantityLabel;
	@FindBy(xpath = "//div[@name=\"move_raw_ids\"]//a[text() = \"Add a line\"]")
	WebElement addALine;
	@FindBy(xpath = "//td[@name=\"product_id\"]//div[@class=\"o_input_dropdown\"]/input")
	WebElement inputProductNameFromComboBox;
	@FindBy(xpath = "//span[@placeholder=\"Manufacturing Reference\"]")
	WebElement newManufacturingOrderPage;
	@FindBy(xpath = "//div[@name=\"state\"]//button[@title=\"Current state\"]")
	WebElement currentStateButton;
	@FindBy(xpath = "//td[@name=\"product_id\"]")
	WebElement componentsProductNameLabel;

	public ManufacturingOrdersPage(WebDriver driver) {
		super(driver);
	}

	@Step("Click Create Button")
	public void clickCreateButton() {
		createButton.click();
	}

	@Step("Input Product Name")
	public void setProductNameTextBox(String productNameLabel) {
		waitElementVisibilityAndFill(productTextBox ,15, productNameLabel);
		waitInSeconds(1);
		productTextBox.sendKeys(Keys.ENTER);
	}

	@Step("Click Add A Line Button")
	public void clickAddALineButton(){
		waitElementAndClick(addALine, 10);
	}

	@Step("Select Product With Name")
	public void selectProductWithName(String productName){
		waitElementVisibilityAndFill(inputProductNameFromComboBox, 15, productName);
		waitInSeconds(1);
		inputProductNameFromComboBox.sendKeys(Keys.ENTER);
	}

	@Step("Click Save Button")
	public void clickSaveButton() {
		saveButton.click();
	}

	@Step("Click Confirm Button")
	public void clickConfirmButton() {
		waitElementAndClick(confirmButton, 10);
	}

	@Step("Click Mask As Done Button")
	public void clickMaskAsDoneButton() {
		waitInSeconds(1);
		waitElementAndClick(maskAsDoneButton, 15);
	}

	@Step("Click Apply Button")
	public void clickApplyButton() {
		waitElementAndClick(applyButton, 10);
	}

	@Step("Check current State")
	public String getCurrentState(){
		// wait for status changed
		waitInSeconds(2);
		return currentStateButton.getText().trim().toLowerCase();
	}

	@Step("Check Product Name Value")
	public String getProductNameValue() {
		return productNameLabel.getText();
	}

	@Step("Check product quantity Value")
	public String getProductQuantityValue() {
		waitElementVisibility(productQuantityLabel, 10);
		return productQuantityLabel.getText().trim();
	}

	@Step("Check Components Product name")
	public String getComponentsProductName(){
		waitElementVisibility(componentsProductNameLabel, 10);
		return componentsProductNameLabel.getText();
	}

	@Step("Check new Manufacturing Order Page is loaded")
	public boolean newManufacturingOrderPageDisplayed(){
		waitElementVisibility(newManufacturingOrderPage, 10);
		return newManufacturingOrderPage.isDisplayed();
	}

	@Step("Manufacturing Page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(manufacturingHome, 20);
		return manufacturingHome.isDisplayed();
	}
}
