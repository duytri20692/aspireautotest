package com.aspire.odoo.enums;

public enum Product {
    //Product Type
    PRODUCT_TYPE_CONSUMABLE("Consumable"),
    PRODUCT_TYPE_SERVICE("Service"),
    PRODUCT_TYPE_STORABLE_PRODUCT("Storable Product"),

    //Product category
    PRODUCT_CATEGORY_ALL("all"),
    PRODUCT_CATEGORY_ALL_EXPENSES("all/expenses"),
    PRODUCT_CATEGORY_ALL_SALEABLE("all/saleable");

    public final String label;

    private Product(String label) {
        this.label = label;
    }
}
