package com.aspire.odoo.tests;

import com.aspire.odoo.models.InventoryProduct;
import com.aspire.odoo.models.LoginUser;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Description;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;
import com.aspire.odoo.helpers.ReadPropertiesFileHelper;
import com.aspire.odoo.pages.MenuPage;
import com.aspire.odoo.pages.LoginPage;
import com.aspire.odoo.pages.InventoryOverviewPage;
import com.aspire.odoo.pages.ManufacturingOrdersPage;
import com.aspire.odoo.pages.ProductsPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class TestAspireApp {
    WebDriver driver;
    LoginPage objLogin;
    MenuPage objMenuPage;
    InventoryOverviewPage objInventoryOverviewPage;
    ManufacturingOrdersPage objManufacturingOrdersPage;
    ProductsPage objProductsPage;
    String configPathFile = "src\\test\\resources\\testData\\Config.properties";
    String loginPathFile = "src\\test\\resources\\testData\\Login.properties";
    String inventoryProductPathFile = "src\\test\\resources\\testData\\InventoryProductInfo.properties";
    String inventoryProductName;
    String manufacturingProductName;
    String internalReference;

    @BeforeTest
    public void beforeTest() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
        driver.get(ReadPropertiesFileHelper.readPropertiesFile(configPathFile,"baseUrl"));
        driver.manage().window().maximize();
    }

    @Test(priority = 0)
    @Description("Login to web application")
    public void loginToWebApplication(){
        objLogin = new LoginPage(driver);
        Assert.assertTrue(objLogin.pageIsLoaded());
        LoginUser user = new LoginUser(ReadPropertiesFileHelper.readPropertiesFile(loginPathFile, "loginUser"),
                ReadPropertiesFileHelper.readPropertiesFile(loginPathFile, "loginPass"));
        objLogin.loginToOdoo(user.getEmail(), user.getPassword());
    }

    @Test(priority = 1)
    @Description("Navigate to `Inventory` feature")
    public void navigateToInventoryFeature(){
        objMenuPage = new MenuPage(driver);
        Assert.assertTrue(objMenuPage.pageIsLoaded());
        objMenuPage.clickInventoryMenu();
    }

    @Test(priority = 2)
    @Description("From the top-menu bar, select `Products -> Products` item, then create a new product")
    public void createANewProduct(){
        objInventoryOverviewPage = new InventoryOverviewPage(driver);
        Assert.assertTrue(objInventoryOverviewPage.pageIsLoaded());
        objInventoryOverviewPage.clickProductsMenu();
        objInventoryOverviewPage.clickProductsItem();
    }

    @Test(priority = 3, dataProvider = "productData")
    @Description("Update the quantity of new product is more than 10")
    public void UpdateProductQuantity(String productName, String productQuantity){
        objProductsPage = new ProductsPage(driver);
        Assert.assertTrue(objProductsPage.pageIsLoaded());
        objProductsPage.clickCreateButton();
        inventoryProductName = ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"productName").trim() + System.currentTimeMillis();
        internalReference = ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"internalReference").trim() + System.currentTimeMillis();
        InventoryProduct product = new InventoryProduct(
                inventoryProductName,
                Boolean.parseBoolean(ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"isCheckedCanBeSold").trim()),
                Boolean.parseBoolean(ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"isCheckedCanBePurchased").trim()),
                ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"productType").trim(),
                ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"unitOfMeasure").trim(),
                ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"purchaseUoM").trim(),
                ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"SalesPrice").trim(),
                ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"Cost").trim(),
                ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"productCategory").trim(),
                internalReference,
                String.valueOf(System.currentTimeMillis() / 1000),
                ReadPropertiesFileHelper.readPropertiesFile(inventoryProductPathFile,"internalNotes").trim());
        objProductsPage.createProductGeneralInformation(product);
        objProductsPage.clickSaveButton();
        //Verify Product created
        Assert.assertEquals(objProductsPage.getCreateProductSuccessMessage(), "Product Template created");
        objProductsPage.clickEditButton();
        objProductsPage.clickUpdateQuantityButton();
        //Verify stock is empty
        Assert.assertEquals(objProductsPage.getEmptyProductMessage(), "Your stock is currently empty");
        objProductsPage.clickCreateQuantityButton();
        objProductsPage.setCountedQuantityTextBox(productQuantity);
        objProductsPage.clickSaveButton();
    }

    @Test(priority = 4)
    @Description("From top-left page, click on `Application` icon")
    public void clickOnApplicationIcon(){
        objProductsPage.clickHomeMenuButton();
    }

    @Test(priority = 5, dataProvider = "productData")
    @Description("Navigate to `Manufacturing` feature, then create a new Manufacturing Order item for the created Product on step #3")
    public void createANewManufacturingOrder(String productName, String productQuantity){
        objMenuPage.clickManufacturingMenu();
        objManufacturingOrdersPage = new ManufacturingOrdersPage(driver);
        Assert.assertTrue(objManufacturingOrdersPage.pageIsLoaded());
        objManufacturingOrdersPage.clickCreateButton();
        //Verify New Manufacturing Order Page is displayed
        Assert.assertTrue(objManufacturingOrdersPage.newManufacturingOrderPageDisplayed(),"New Manufacturing Order Page does not display");
        manufacturingProductName = productName + System.currentTimeMillis() / 1000;
        objManufacturingOrdersPage.setProductNameTextBox(manufacturingProductName);
        //Add created Product on step #3
        objManufacturingOrdersPage.clickAddALineButton();
        objManufacturingOrdersPage.selectProductWithName(inventoryProductName);
        objManufacturingOrdersPage.clickSaveButton();
    }

    @Test(priority = 6)
    @Description("Update the status of new Orders to “Done” successfully")
    public void updateOrderStatusToDone(){
        objManufacturingOrdersPage.clickConfirmButton();
        objManufacturingOrdersPage.clickMaskAsDoneButton();
        objManufacturingOrdersPage.clickApplyButton();
        //Verify the current state = 'Done'
        Assert.assertEquals(objManufacturingOrdersPage.getCurrentState(), "done");
    }

    @Test(priority = 7, dataProvider = "productData")
    @Description("Validate the new Manufacturing Order is created with corrected information.")
    public void ValidateCreatedManufacturingOrder(String productName, String productQuantity){
        Assert.assertTrue(objManufacturingOrdersPage.getProductNameValue().equals(manufacturingProductName));
        Assert.assertEquals(objManufacturingOrdersPage.getProductQuantityValue(), productQuantity);
        Assert.assertEquals(objManufacturingOrdersPage.getComponentsProductName(), "[" + internalReference + "] " + inventoryProductName);
    }

    @DataProvider
    public Object[][] productData() {
        return new Object[][]{
                {"Manufacturing Order", "16"}
        };
    }

    @AfterTest
    public void afterTest() {
        driver.close();
    }
}
