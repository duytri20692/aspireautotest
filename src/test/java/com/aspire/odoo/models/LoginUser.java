package com.aspire.odoo.models;

public class LoginUser {
	String email;
	String pass;
	
	public LoginUser(String email, String pass) {
		this.email = email;
		this.pass = pass;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return pass;
	}
	
	public void setPassword(String password) {
		this.pass = password;
	}
}
