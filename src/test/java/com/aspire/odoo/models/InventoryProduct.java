package com.aspire.odoo.models;

public class InventoryProduct {
    private String productName;
    private boolean isCheckedCanBeSold;
    private boolean isCheckedCanBePurchased;
    //General Information
    private String productType;
    private String unitOfMeasure;
    private String purchaseUoM;
    private String SalesPrice;
    private String Cost;
    private String productCategory;
    private String internalReference;
    private String barcode;
    private String internalNotes;
    //Inventory
    private boolean isCheckedRoutes;
    private String tracking;
    private String responsible;
    private String weight;
    private String volume;
    private String manufacturingLeadTime;
    private String customerLeadTime;
    private String descriptionForReceipts;
    private String descriptionForDeliveryOrders;
    private String descriptionForInternalTransfers;

    public InventoryProduct(String productName, boolean isCheckedCanBeSold, boolean isCheckedCanBePurchased, String productType, String unitOfMeasure, String purchaseUoM, String salesPrice, String cost, String productCategory, String internalReference, String barcode, boolean isCheckedRoutes, String tracking, String responsible, String weight, String volume, String manufacturingLeadTime, String customerLeadTime, String descriptionForReceipts, String descriptionForDeliveryOrders, String descriptionForInternalTransfers) {
        this.productName = productName;
        this.isCheckedCanBeSold = isCheckedCanBeSold;
        this.isCheckedCanBePurchased = isCheckedCanBePurchased;
        this.productType = productType;
        this.unitOfMeasure = unitOfMeasure;
        this.purchaseUoM = purchaseUoM;
        SalesPrice = salesPrice;
        Cost = cost;
        this.productCategory = productCategory;
        this.internalReference = internalReference;
        this.barcode = barcode;
        this.isCheckedRoutes = isCheckedRoutes;
        this.tracking = tracking;
        this.responsible = responsible;
        this.weight = weight;
        this.volume = volume;
        this.manufacturingLeadTime = manufacturingLeadTime;
        this.customerLeadTime = customerLeadTime;
        this.descriptionForReceipts = descriptionForReceipts;
        this.descriptionForDeliveryOrders = descriptionForDeliveryOrders;
        this.descriptionForInternalTransfers = descriptionForInternalTransfers;
    }

    public InventoryProduct(String productName, boolean isCheckedCanBeSold, boolean isCheckedCanBePurchased, String productType, String unitOfMeasure, String purchaseUoM, String salesPrice, String cost, String productCategory, String internalReference, String barcode, String internalNotes) {
        this.productName = productName;
        this.isCheckedCanBeSold = isCheckedCanBeSold;
        this.isCheckedCanBePurchased = isCheckedCanBePurchased;
        this.productType = productType;
        this.unitOfMeasure = unitOfMeasure;
        this.purchaseUoM = purchaseUoM;
        SalesPrice = salesPrice;
        Cost = cost;
        this.productCategory = productCategory;
        this.internalReference = internalReference;
        this.barcode = barcode;
        this.internalNotes = internalNotes;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public boolean isCheckedCanBeSold() {
        return isCheckedCanBeSold;
    }

    public void setCheckedCanBeSold(boolean checkedCanBeSold) {
        isCheckedCanBeSold = checkedCanBeSold;
    }

    public boolean isCheckedCanBePurchased() {
        return isCheckedCanBePurchased;
    }

    public void setCheckedCanBePurchased(boolean checkedCanBePurchased) {
        isCheckedCanBePurchased = checkedCanBePurchased;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getPurchaseUoM() {
        return purchaseUoM;
    }

    public void setPurchaseUoM(String purchaseUoM) {
        this.purchaseUoM = purchaseUoM;
    }

    public String getSalesPrice() {
        return SalesPrice;
    }

    public void setSalesPrice(String salesPrice) {
        SalesPrice = salesPrice;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String cost) {
        Cost = cost;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getInternalReference() {
        return internalReference;
    }

    public void setInternalReference(String internalReference) {
        this.internalReference = internalReference;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getInternalNotes() {
        return internalNotes;
    }

    public void setInternalNotes(String internalNotes) {
        this.internalNotes = internalNotes;
    }

    public boolean isCheckedRoutes() {
        return isCheckedRoutes;
    }

    public void setCheckedRoutes(boolean checkedRoutes) {
        isCheckedRoutes = checkedRoutes;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getManufacturingLeadTime() {
        return manufacturingLeadTime;
    }

    public void setManufacturingLeadTime(String manufacturingLeadTime) {
        this.manufacturingLeadTime = manufacturingLeadTime;
    }

    public String getCustomerLeadTime() {
        return customerLeadTime;
    }

    public void setCustomerLeadTime(String customerLeadTime) {
        this.customerLeadTime = customerLeadTime;
    }

    public String getDescriptionForReceipts() {
        return descriptionForReceipts;
    }

    public void setDescriptionForReceipts(String descriptionForReceipts) {
        this.descriptionForReceipts = descriptionForReceipts;
    }

    public String getDescriptionForDeliveryOrders() {
        return descriptionForDeliveryOrders;
    }

    public void setDescriptionForDeliveryOrders(String descriptionForDeliveryOrders) {
        this.descriptionForDeliveryOrders = descriptionForDeliveryOrders;
    }

    public String getDescriptionForInternalTransfers() {
        return descriptionForInternalTransfers;
    }

    public void setDescriptionForInternalTransfers(String descriptionForInternalTransfers) {
        this.descriptionForInternalTransfers = descriptionForInternalTransfers;
    }
}
