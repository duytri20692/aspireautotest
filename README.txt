PRE-CONDITION:
- Installed JAVA (Set up JAVA_HOME, add Jdk home to Path)
- Installed Maven
- Installed allure report (Refer: https://docs.qameta.io/allure/)

HOW TO RUN
1. Go to the project folder: cd:/Aspire
2. Run: mvn test

VIEW REPORT:
1. Generate new report: Run: allure generate --clean
2. Open the new report: Run: allure open